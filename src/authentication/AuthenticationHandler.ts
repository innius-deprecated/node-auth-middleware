import {authentication} from "@toincrease/node-sdk";
import {MiddlewareRequestContext, Claim, InvalidCredentialsError, UnauthorizedError} from "@toincrease/node-context";
/**
 * Returns middleware handler for authentication 
 * 
 * @param {AuthenticationApi} client is the authentication client for performing authentication requests 
 * @returns {(c: MiddlewareRequestContext) => void} (description)
 */
export function AuthenticationHandler(client: authentication.AuthenticationApi): (c: MiddlewareRequestContext) => void {
    return function(c: MiddlewareRequestContext): void {
        // get the authorization header from the request            
        let tokenString: string = c.header("authorization").value;
        if (!tokenString) {
            return c.next(new InvalidCredentialsError("authorization header is not provided or does not include a valid token"));
        }        
        let token = parseAuthenticationHeader(tokenString);
        if (!token) {
            return c.next(new InvalidCredentialsError("Format is Authorization: Bearer [token]"));
        }

        // invoke the authentication client     
        try {
            client.validateToken(c, { token: token })            
                .map(x => { 
                    if (x.statusCode === 403 || x.statusCode === 401) {
                        throw new UnauthorizedError(x.statusMessage);
                    };                
                    return convertToClaim(x);
                })                  
                .subscribe(x => c.setClaim(x), err => c.next(err), () => c.next());
        } catch (err) {
            c.next(err);
        }
    };
}

/**
 * Converts ValidateTokenOuput to a Claim 
 * 
 * @param {ValidateTokenOutput} x the response from the authentication service
 * @returns {Claim} 
 */
function convertToClaim(x: authentication.ValidateTokenOutput): Claim {
    var claim = new Claim();

    claim.audience = x.aud;
    claim.userMetadata = {
        companyId: x.user_metadata.companyid,
        userId: x.user_metadata.userid,
        domain: x.user_metadata.domain
    };
    claim.issuer = x.iss;
    claim.expiresAt = x.exp;
    claim.issuedAt = x.iat;
    claim.scopes = x.scp;

    return claim;
}

/**
 * gets the token from the authorization header
 * the format of the header is [bearer] token 
 * @param {string} tokenString the unparsed token string
 * @returns {string} the token string without the scheme  
 */
function parseAuthenticationHeader(tokenString: string): string {
    let parts = tokenString.split(" ");
    if (parts.length === 2) {
        let scheme = parts[0];
        let token = parts[1];

        if (/^Bearer$/i.test(scheme)) {
            return token;
        }
    }
}
