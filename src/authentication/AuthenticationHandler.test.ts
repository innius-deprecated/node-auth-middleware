import {Mock, It} from "typemoq";
import {assert} from "chai";
import {AuthenticationHandler} from "./AuthenticationHandler";
import {authentication} from "@toincrease/node-sdk";
import {newTestRequestContextBuilder, MiddlewareRequestContext} from "@toincrease/node-context";
import {Observable} from "@reactivex/rxjs";


describe("AuthenticationHandler", () => {
    describe("authenticate", () => {
        const response: authentication.ValidateTokenOutput = {
            "aud": "tGvsCT4SRnIAMcpUvhFHudZZDAor42fz",
            "exp": 1459279958,
            "iat": 1459243958,
            "iss": "https://to-increase.auth0.com/",
            "sub": "auth0|56e811294a315645777e5072",
            "user_metadata": {
                "companyid": "plantage",
                "userid": "ronvdw"
            },
            "scp": ["view", "edit"]
        }
        let client = new authentication.AuthenticationClient();
        let mock = Mock.ofInstance(client);
        mock.setup(x => x.validateToken(It.isAny(), It.isAny())).returns(() => Observable.of(response));

        describe("request without bearer token", () => {
            let c = newTestRequestContextBuilder().build();
            let handler = AuthenticationHandler(mock.object)
            handler(c);            
            it("should return a 401", () => {
                assert.equal(c.testResponse().getStatus(), 401);
            });
        })

        describe("request with invalid authentication header", () => {
            let c = newTestRequestContextBuilder()
                .withHeader("authorization", "bearer")
                .build();
                
            let handler = AuthenticationHandler(mock.object)
            handler(c);
            it("should return a 401", () => {
                assert.equal(c.testResponse().getStatus(), 401);
            });
        })

        describe("with unauthorized error", () => {
            let c = newTestRequestContextBuilder()
                .withHeader("authorization", "bearer valid_token_string")
                .build();
            
            let mock = Mock.ofInstance(client);    
            let resp : authentication.ValidateTokenOutput = {
                statusCode : 403, 
                statusMessage : "you're not authorized"
            }            
            mock.setup(x => x.validateToken(It.isAny(), It.isAny())).returns(() => Observable.of(resp));
            let handler = AuthenticationHandler(mock.object)
            handler(c);
            it("should return a 401", () => {
                assert.equal(c.testResponse().getStatus(), 401);                
            });            
        })

        describe("with server error", () => {
            let c = newTestRequestContextBuilder()
                .withHeader("authorization", "bearer valid_token_string")
                .build();
            let mock = Mock.ofInstance(client);    
            mock.setup(x => x.validateToken(It.isAny(), It.isAny())).throws(new Error("server error"));
            let handler = AuthenticationHandler(mock.object)
            handler(c);
            it("should return a 500", () => {
                assert.equal(c.testResponse().getStatus(), 500);
            });
        })
        
        describe("request with valid bearer token", () => {            
            let c = newTestRequestContextBuilder()
                .withHeader("authorization", "bearer valid_token_string")
                .build();
            let handler = AuthenticationHandler(mock.object)
            handler(c);
            
            it('the request should have valid claims', () => {
                assert.equal(c.claim().userMetadata.companyId, response.user_metadata.companyid);
            })
            
        })

    })
})

