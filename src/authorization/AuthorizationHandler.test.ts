
import {Mock, It} from "typemoq";
import {assert} from "chai";
import {TRN} from "@toincrease/node-trn";
import {AuthorizationHandler, Action} from "./AuthorizationHandler";
import {authorization} from "@toincrease/node-sdk";
import {newTestRequestContextBuilder, MiddlewareRequestContext, RequestContext, Claim} from "@toincrease/node-context";
import {Observable} from "@reactivex/rxjs";

describe("AuthorzationHandler", () => {
    describe("#authorize", () => {
        let resource = TRN.parse("trn:plantage:machine:1234/collateral");
        let client = new authorization.AuthorizationClient();
        let mock = Mock.ofInstance(client);
        let subject: TRN;

        let effect: authorization.Effect;
        mock.setup(x => x.authorize(It.isAny(), It.isAny())).returns((x: any, input: authorization.AuthorizationInput) => {
            var result: authorization.AuthorizationOutput = {
                action: input.action,
                subject: input.subject,
                resource: input.resource,
                effect: effect,
            };
            subject = input.subject;
            return Observable.of(result);
        });

        describe("allowed", () => {
            effect = authorization.Effect.allow;
            let claim = new Claim();
            claim.userMetadata.companyId = "plantage";
            claim.userMetadata.userId = "ronvdw";
            let c = newTestRequestContextBuilder().withClaim(claim).build();
            let handler = AuthorizationHandler(mock.object, resource, Action.edit);

            handler(c);
            it("should not return a 401", () => {
                assert.equal(c.testResponse().getStatus(), undefined);
            });
        })

        describe("denied", () => {
            let claim = new Claim();
            claim.userMetadata.companyId = "plantage";
            claim.userMetadata.userId = "ronvdw";

            effect = authorization.Effect.deny;
            let c = newTestRequestContextBuilder()
                .withClaim(claim)
                .build();
            let handler = AuthorizationHandler(mock.object, resource, Action.edit);
            handler(c);

            it("should return a 401", () => {
                assert.equal(c.testResponse().getStatus(), 403);
            });

        })

        describe("subject from context", () => {
            it("with user claim", () => {
                let claim = new Claim();
                claim.userMetadata.companyId = "plantage";
                claim.userMetadata.userId = "ronvdw";
                let c = newTestRequestContextBuilder().withClaim(claim).build();
                AuthorizationHandler(mock.object, resource, Action.edit)(c)

                it("the subject should be the user trn", () => {
                    assert.equal(subject.toString(), "trn:plantage:person:ronvdw")
                })
            })
            
            it("without user claim", () => {
                let claim = new Claim();
                claim.userMetadata.companyId = "plantage";
                let c = newTestRequestContextBuilder().withClaim(claim).build();
                AuthorizationHandler(mock.object, resource, Action.edit)(c)

                it("the subject should be the company trn", () => {
                    assert.equal(subject.toString(), "trn:plantage:company:plantage")
                })
            })
        })

    });
});

