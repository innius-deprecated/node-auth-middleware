import {authorization, authentication} from "@toincrease/node-sdk";
import {MiddlewareRequestContext, NotAuthorizedError} from "@toincrease/node-context";
import {TRN, ResourceType} from "@toincrease/node-trn";

export enum Action {
    view,
    edit,
    create,
    delete
}

export function AuthorizationHandler(client: authorization.AuthorizationApi, resource: TRN, action: Action):
    (c: MiddlewareRequestContext) => void {
    return function(c: MiddlewareRequestContext): void {
        let input = new authorization.AuthorizationInput(getSubjectFromContext(c), resource, Action[action]);

        let result: authorization.AuthorizationOutput;
        client.authorize(c, input).subscribe(
            x => result = x,
            err => c.next(err),
            () => {
                c.logger().debug("authorization result: " + JSON.stringify(result));
                if (result.effect === authorization.Effect.deny) {
                    return c.next(new NotAuthorizedError(
                        `${input.action} for ${resource.toString()} ${authorization.Effect[result.effect]}`));
                }
                c.next();
            });
    };
}

function getSubjectFromContext(c: MiddlewareRequestContext): TRN {
    let company = c.claim().userMetadata.companyId;
    if (c.claim().hasUserId()) {
        return new TRN(company, ResourceType.person, c.claim().userMetadata.userId);
    }
    return new TRN(company, ResourceType.company, company);
}

